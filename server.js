const app = require('./app')
const http = require('http')
require('./db')
const server = http.createServer(app)
const Port = 8080;
const Host = '0.0.0.0';
server.listen(Port, Host, () => console.log(`server in running on port: ${Port} host: ${Host}`));

