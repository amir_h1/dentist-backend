const express = require('express')
const app = express()
const morgan = require('morgan')
const bodyParser = require('body-parser')
const bill = require('./routes/bill')
const ex = require('./routes/export')
const schedul = require('./routes/schedul')

app.use(morgan('dev'))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

app.use('/api/v1/bill', bill)
app.use('/api/v1/ex', ex)
app.use('/api/v1/schedul', schedul)

app.use((req, res) => {
    res.status(404).json({ result: 'not Found' })
})

module.exports = app