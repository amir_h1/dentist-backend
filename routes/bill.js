const app = require('express')()
const db = require('../db').db('dentist').collection('bilss')
const dbScheduls = require('../db').db('dentist').collection('scheduls')
const OI = require('mongodb').ObjectID

app.post('/', async (req, res) => {
    const time = req.body.time;
    const some = req.body.some;
    const customerName = req.body.customerName
    const bills = req.body.bills;
    if (!bills) return res.status(500).json({ result: 'bills most not be null' })
    const input = { time, some, customerName, bills }
    await db.insertOne(input);

    res.status(201).json({ result: 'added' })
})

app.get('/', async (req, res) => {
    let bills = []
    let some = 0;
    await db.find({}).forEach(f => {
        bills.push(f);
        some += f.some;
    })
    res.status(200).json({
        result: { count: bills.length, some: some, bills: bills }
    })
})
app.delete('/', async (req, res) => {
    const id = { _id: OI(req.body.id) }
    const find = await db.findOne(id);
    if (!find) return res.status(404).json({ result: 'not found' })
    const find2 = await dbScheduls.findOne(id)
    if (find2) {
        await dbScheduls.deleteOne(id);
    }
    await db.deleteOne(id)
    res.status(201).json({ result: 'deleted' })
})
module.exports = app;