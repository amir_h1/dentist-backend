const app = require('express')()
const dbBill = require('../db').db('dentist').collection('bilss')
const dbScheduls = require('../db').db('dentist').collection('scheduls')
const OI = require('mongodb').ObjectID

app.post('/', async (req, res) => {
    const id = { _id: OI(req.body.id) }
    const find = await dbBill.findOne(id);
    if (!find) return res.status(404).json({ result: 'not found' })
    const find2 = await dbScheduls.findOne(id)
    if (find2) return res.status(500).json({ result: 'dupKey' })
    await dbScheduls.insertOne(find)
    res.status(201).json({ result: 'added to schedules' })
})
app.get('/', async (req, res) => {
    var list = []
    const find = await dbScheduls.find({}).forEach(f => list.push(f));
    res.status(200).json({ result: { count: list.length, list: list } })
})
module.exports = app