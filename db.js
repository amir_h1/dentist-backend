const Mongodb = require('mongodb').MongoClient;
const Assert = require('assert');

const url = 'mongodb://admin:admin@217.219.162.177/';

const client = new Mongodb(url, { useUnifiedTopology: true });
client.connect((err) => {
    Assert.equal(null, err);
    console.log('connected to mongoDB');
});
module.exports = client;
